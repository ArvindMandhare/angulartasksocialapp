import { Directive, ElementRef,HostListener, Input ,Output,EventEmitter} from '@angular/core';
import {PostService} from '../Service/post.service';
import { Post } from '../Model/post';

@Directive({
  selector: '[appLike]'
})
export class LikeDirective {
  @Output() isLiked = new EventEmitter<boolean>();
  constructor( private e1:ElementRef,
  private postService:PostService) { }
@Input('post') inputPost:Post;
@Input() isLike :boolean;

// @HostListener('load') onPageLoad()
// {
//   console.log('on load');
//   if(!this.isLike)
//   {
//    this.e1.nativeElement.src='assets/like.png';
//   }
//   else
//   {
//    this.e1.nativeElement.src='assets/like1.png';
//   } 
// }
@HostListener('click') onclick(){
 if(!this.isLike)
 {
  this.e1.nativeElement.src='assets/like.png';
this.isLike=true;
this.inputPost.likedUsers.push(JSON.parse(localStorage.getItem('currentUser')).userName);
this.inputPost.likesCounter++;
 }
 else
 {
  this.e1.nativeElement.src='assets/like1.png';
  this.isLike=false;
  this.inputPost.likedUsers.splice(this.inputPost.likedUsers.indexOf(JSON.parse(localStorage.getItem('currentUser')).userName),1);
  this.inputPost.likesCounter!==0 ? this.inputPost.likesCounter-- :0;
 }
 this.postService.UpdatePost(this.inputPost).subscribe();
 //this.isLiked.emit(this.isLike);
}
}
