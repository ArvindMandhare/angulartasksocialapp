import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {FormsModule} from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './Components/header/header.component';
import { LoginComponent } from './Components/login/login.component';
import { HomeComponent } from './Components/home/home.component';
import { RegisterComponent } from './Components/register/register.component';
import {DataPassService} from './Service/data-pass.service';
import { ProfileComponent } from './Components/profile/profile.component';
import { DefaultLandingComponent } from './Components/default-landing/default-landing.component';
import { PostComponentComponent } from './Components/post-component/post-component.component';
import { LikeDirective } from './Directive/like.directive';
import { CommentComponent } from './Components/comment/comment.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    LoginComponent,
    HomeComponent,
    RegisterComponent,
    ProfileComponent,
    DefaultLandingComponent,
    PostComponentComponent,
    LikeDirective,
    CommentComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
