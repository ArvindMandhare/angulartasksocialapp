import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import{ HomeComponent } from '../app/Components/home/home.component';
import {LoginComponent} from '../app/Components/login/login.component';
import {RegisterComponent} from '../app/Components/register/register.component';
import {ProfileComponent} from '../app/Components/profile/profile.component';
import {DefaultLandingComponent} from '../app/Components/default-landing/default-landing.component';
const routes: Routes = [
  {path:'',component:DefaultLandingComponent},
  {path:'login',component:LoginComponent},
  {path:'register',component:RegisterComponent},
  {path:'home',component:HomeComponent},
  {path:'profile',component:ProfileComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
