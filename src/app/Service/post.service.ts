import { Injectable } from '@angular/core';
import {Post} from '../Model/post';
import {Comment} from '../Model/comment';
import { Observable , of} from 'rxjs';
import { max } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class PostService{

  constructor() { }

  getPost():Observable<Post[]>
  {
   let Posts =JSON.parse(localStorage.getItem('Posts'));
   return of(Posts);
  }

  SavePost(post:Post):Observable<Post>
  {
    let posts: Post [] =JSON.parse(localStorage.getItem('Posts'));
    
    //posts.find(p=>p.id== max(post.id));
    if (posts ===null)
    {
      posts= new Array<Post>();
      
    }
    posts.sort(p=>p.id);
    post.id = posts.length!==0 ? posts[posts.length-1].id + 1 : 1;
    post.likedUsers=new Array<string>();
    post.likesCounter=0;
    post.commments=new Array<Comment>();
    posts.push(post);
    localStorage.setItem('Posts',JSON.stringify( posts)) ;
    return of (post);
  }

  UpdatePost(post:Post):Observable<Post>
  {
    let posts: Post [] =JSON.parse(localStorage.getItem('Posts'));
    //let postIn = posts.find(p=>p.id==post.id).likedUsers.filter();
    //posts.filter(p=>p.id!==post.id);
    posts.splice(posts.indexOf(post),1,post);
    localStorage.setItem('Posts',JSON.stringify(posts));
    //posts.push(post)
    return of (post);
  }
}
