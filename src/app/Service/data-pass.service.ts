import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import {User} from '../Model/user';
@Injectable(
  {
    providedIn: 'root'
  }
)
export class DataPassService {
//data = new EventEmitter();
private messageSource = new BehaviorSubject(new User);
currentData = this.messageSource.asObservable();
  constructor() { }

  // sendData(data:any):Observable<any>{
  //  return of(this.data.emit(data));
  // }

  sendDataq(data1:User)
  {
    this.messageSource.next(data1);
    console.log('Dt service  current data'+ this.currentData);
    console.log('Dt service  old  data'+ this.messageSource);
  }
}
