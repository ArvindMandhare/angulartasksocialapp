import { Injectable } from '@angular/core';
import {User} from '../Model/user';
import {Observable, observable,of} from 'rxjs';
import {HttpClient,HttpHeaders} from '@angular/common/http';
import {map,tap,catchError} from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class RegisterService {
 // user1 :User;
  constructor() { }
  RegisterUser(user:User):Observable<User>
  {

    localStorage.setItem(`${user.userName}`, JSON.stringify(user));
   var user1  = JSON.parse(localStorage.getItem(`${user.userName}`));
  return of(user1);
  }

 
}
