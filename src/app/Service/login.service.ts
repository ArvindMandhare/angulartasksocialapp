import { Injectable } from '@angular/core';
import {Observable, of} from 'rxjs';
import {HttpClient,HttpHeaders} from '@angular/common/http';
import {map,tap,catchError} from 'rxjs/operators';
import {User} from '../Model/user';
@Injectable({
  providedIn: 'root'
})
export class LoginService {
  //private RegisterUrl='api/users';
  constructor() { }

  Login(userName :string, password:string):Observable<any>
  {
        //const url =`${this.RegisterUrl}/${name}/${password}`;

  //       let myParams = new URLSearchParams();
  //   myParams.set('name', name);
  // myParams.set('password', password);
  // let options = new RequestOptions({ headers: myHeaders, params: myParams });
    //return this.httpClient.get<User>(url);
let user = JSON.parse(localStorage.getItem(userName));
localStorage.setItem('currentUser', JSON.stringify(user));
   return of(user);
  }
}
