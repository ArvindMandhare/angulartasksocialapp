import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import{DataPassService} from '../../Service/data-pass.service';
import { User } from '../../Model/user';
import {Post} from '../../Model/post';
import {Comment} from '../../Model/comment';
import {PostComponentComponent} from '../post-component/post-component.component';
import {PostService} from '../../Service/post.service';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
user:User;
post:Post={
  id:null,
  userName:null,
  content:null,
  likesCounter:null,
  likedUsers:null,
  commments:null
};

result:boolean=false;
  constructor(private dataservice:DataPassService,
  private postService:PostService) { }
  posts;
  ngOnInit() {
    // this.dataservice.currentData.subscribe(message=>this.user=message);
    // console.log(this.user.name);
    //let posts=new Array<Post>();  
    this.GetPost();
     
  }
GetPost()
{
  this.postService.getPost().subscribe(posts=>this.posts=posts);
  console.log(this.posts);
}

  submitPost()
  {
    let user :User= JSON.parse(localStorage.getItem('currentUser'));
    this.post.userName=user.userName;
    //this.post=null;
    this.postService.SavePost(this.post).subscribe(
      (post=>this.post),
      (err=>console.log(err)),
      (()=>{
        this.result=true;
        this.GetPost();
      })
    );
  }
}
