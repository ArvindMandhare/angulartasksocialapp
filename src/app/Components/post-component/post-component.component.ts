import { Component, OnInit,Input } from '@angular/core';
import {PostService} from '../../Service/post.service';
import { Post } from '../../Model/post';
import {LikeDirective} from '../../Directive/like.directive';
import {Comment} from '../../Model/comment';
@Component({
  selector: 'app-post-component',
  templateUrl: './post-component.component.html',
  styleUrls: ['./post-component.component.css']
})
export class PostComponentComponent implements OnInit {
 @Input() post:Post;
 islike:boolean=false;
 srcPath: string;
 comment:Comment={
   content:null,
   userName:null
 };
 result:boolean;
  constructor(private postService:PostService) { }

  ngOnInit() {
    if(this.post.likedUsers.includes(JSON.parse(localStorage.getItem('currentUser')).userName))
    {
      this.islike=true;
      this.srcPath='assets/like.png';
    }
    else
    {
      this.srcPath='assets/like1.png';
    }
  //   this.posts=new Array<Post>();    
  //  this.posts = this.postService.getPost().subscribe();
  }
  onchkIsLiked(liked:boolean)
  {
    this.islike=liked;
  }

  submitComment()
  {
    this.comment.userName=JSON.parse(localStorage.getItem('currentUser')).userName;
    this.post.commments.push(this.comment);
    this.postService.UpdatePost(this.post).subscribe((p=>this.post=p),
    (err=>console.log('Error')),(()=>{
      this.result=true;
      
    }
    ));
  }
}
