import { Component, OnInit } from '@angular/core';
import {User} from '../../Model/user';
@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
user:User;
imgSer:string;
  constructor() { }

  ngOnInit() {
    this.user=JSON.parse(localStorage.getItem('currentUser'));  
    this.imgSer=   "data:image/png;base64," + this.user.userImage;
  }

}
