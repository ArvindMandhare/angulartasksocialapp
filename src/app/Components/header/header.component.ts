import { Component, OnInit } from '@angular/core';
import {DataPassService} from '../../Service/data-pass.service';
import { User } from '../../Model/user';
//import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  user: User={
    name:null,
    mobile:null,
    email:null,
    userName:null,
    password:null,
    userImage:null
  };
  imgSer:string;
  constructor(private dataService:DataPassService//,
   // private _sanitizer: DomSanitizer
  ) { }

  ngOnInit() {
    let currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if (currentUser == null)
    {
    this.dataService.currentData.subscribe(message=>this.user=message);
    console.log(this.user.name);
    }
    else if (currentUser.name == undefined)
    {
    this.dataService.currentData.subscribe(message=>this.user=message);
    console.log(this.user.name);
    }
else{
  this.user=currentUser;
  let userImage =this.user.userImage.replace('data:image/jpeg;base64,/', "");
  //this.imgSer= this._sanitizer.bypassSecurityTrustResourceUrl(this.user.userImage);
  this.imgSer="data:image/jpeg;base64," + userImage;
}
  }

  logout()
  {
    localStorage.removeItem('currentUser');
  }

}
