import { Component, OnInit } from '@angular/core';

import {Router} from '@angular/router';
import {User} from '../../Model/user';
import {LoginService} from '../../Service/login.service';
import {DataPassService} from '../../Service/data-pass.service';
//import { Subscription } from 'rxjs';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  LoginDetails={
    userName:null,
    password:null
  };
  message:string;
  //sub:Subscription;
  user: User={
    name:null,
    mobile:null,
    email:null,
    userName:null,
    password:null,
    userImage:null
  };
  constructor( private loginService:LoginService,
  private router:Router,
private dataPassService:DataPassService) { }

  ngOnInit() {
    //this.sub= this.route.data.subscribe(v=>this.dataPassService.sendData(this.LoginDetails));
    this.dataPassService.currentData.subscribe(d=>this.user=d);
  }
Login():void
{
  this.loginService.Login(this.LoginDetails.userName, this.LoginDetails.password).subscribe(userdetails=>this.user=userdetails)
  if (this.user!=null){
    //console.log(this.user);
  this.dataPassService.sendDataq(this.user);//(v=>this.dataPassService.sendData(this.user));
    this.router.navigateByUrl('/home');
  }
  else
  {
    this.router.navigateByUrl('/register');
  }

}
// ngOnDestroy() {
//     this.sub.unsubscribe();
//   }
}
