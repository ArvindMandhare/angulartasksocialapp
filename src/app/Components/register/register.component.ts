import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {User} from '../../Model/user';
import {RegisterService} from '../../Service/register.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  user: User={
    name:null,
    mobile:null,
    email:null,
    userName:null,
    password:null,
    userImage:null
  };
  constructor(private registerService:RegisterService,
  private router:Router) { }

  ngOnInit() {
  }
  fileToUpload: File = null;
url: any;
  handleFileInput(event) {
    //this.fileToUpload = files.item(0);
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();
  
      reader.onload = (event: ProgressEvent) => {
        this.url = (<FileReader>event.target).result;
      }
  
      reader.readAsDataURL(event.target.files[0]);
    }
    //document.getElementById('userImage').setAttribute('src',eve.target.value);
}
  // readURL(input) 
  // {
  //     document.getElementById("userImage").style.display = "block";
  
  //     if (input.files && input.files[0]) {
  //         var reader = new FileReader();
  
  //         reader.onload = function (e) {
  //             document.getElementById('userImage').src =  e.target.result;
  //         }
  
  //         reader.readAsDataURL(input.files[0]);
  //     }
  // }
  getBase64Image() {
    //elementImg : HTMLImageElement;
   var elementImg =<HTMLImageElement> document.getElementById('userImage'); 
   // elementImg.setAttribute('src',`assets/${this.user.userName}.png`);
   var canvas = document.createElement("canvas");
   //canvas.width = parseInt (elementImg.getAttribute('width'));
   //canvas.height = parseInt (elementImg.getAttribute('height')) ;

   var ctx = canvas.getContext("2d");
   ctx.drawImage(elementImg, 0, 0);

   var dataURL = canvas.toDataURL("image/png");

   return dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
}
  register():void{
    this.user.userImage=this.getBase64Image();
    this.registerService.RegisterUser(this.user).subscribe(userdetails => this.user=userdetails);
    if(this.user!=null)
{
this.router.navigateByUrl('/login');
}
else
{
  this.router.navigateByUrl('/home');
}
  }
}
