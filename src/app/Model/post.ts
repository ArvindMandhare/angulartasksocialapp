import {Comment} from './comment';
import { User } from './user';
export class Post {
    id:number
    content:string
    likesCounter:number
    likedUsers:string[]
    commments:Comment[]
    userName:string
}
